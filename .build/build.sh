#!/bin/bash

set -e

export LC_ALL=C
export DEBIAN_FRONTEND=noninteractive
aptInstall='apt-get install -y --no-install-recommends'

apt-get update

$aptInstall mariadb-client
apt autoremove -y
echo -e "Host *\n\tStrictHostKeyChecking no\n\tUserKnownHostsFile=/dev/null\n" >> /etc/ssh/ssh_config
