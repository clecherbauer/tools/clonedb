FROM registry.gitlab.com/clecherbauer/docker-images/debian:bullseye

USER root

COPY clone-db /usr/local/bin/clonedb
COPY .build /.build
RUN /.build/build.sh

EXPOSE 80
