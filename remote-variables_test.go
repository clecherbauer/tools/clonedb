package main

import (
	"reflect"
	"testing"
)

func TestRemoteVariableParserCanParseModeOne(t *testing.T) {
	input := "dbhost:root:root:database"
	expected := remoteVariables{}
	expected.mode = 1
	expected.database.host = "dbhost"
	expected.database.user = "root"
	expected.database.password = "root"
	expected.database.name = "database"

	actually := remoteVariableParser(input)

	if !reflect.DeepEqual(expected, actually) {
		t.Error("TestRemoteVariableParserCanParseModeOne was incorrect")
	}
}

func TestRemoteVariableParserCanParseModeTwo(t *testing.T) {
	input := "user@remote:root:root:database"
	expected := remoteVariables{}
	expected.mode = 2
	expected.host = "remote"
	expected.user = "user"
	expected.database.host = "localhost"
	expected.database.user = "root"
	expected.database.password = "root"
	expected.database.name = "database"
	actually := remoteVariableParser(input)

	if !reflect.DeepEqual(expected, actually) {
		t.Error("TestRemoteVariableParserCanParseModeTwo was incorrect")
	}
}

/**
 * For Fixtures take a look at tests/mock/ssh
 */
func TestRemoteVariableParserCanParseModeFour(t *testing.T) {
	input := "user@remote:/var/comsolit/project/branch/.env"
	expected := remoteVariables{}
	expected.mode = 3
	expected.host = "remote"
	expected.user = "user"
	expected.database.host = "localhost"
	expected.database.user = "User"
	expected.database.password = "TEST1234"
	expected.database.name = "TEST"
	expected.dotenv = "/var/comsolit/project/branch/.env"
	actually := remoteVariableParser(input)

	if !reflect.DeepEqual(expected, actually) {
		t.Error("TestRemoteVariableParserCanParseModeFour was incorrect")
	}
}

/**
 * For Fixtures take a look at tests/mock/ssh
 */
func TestRemoteVariableParserCanParseModeFive(t *testing.T) {
	input := "test.sql"
	expected := remoteVariables{}
	expected.mode = 5
	expected.filepath = "test.sql"
	actually := remoteVariableParser(input)

	if !reflect.DeepEqual(expected, actually) {
		t.Error("TestRemoteVariableParserCanParseModeFive was incorrect")
	}
}
