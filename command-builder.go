package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func buildCommand(remoteVariables remoteVariables, target bool) *exec.Cmd {
	arguments := []string{}

	if target && !isForceEnabled() {
		askForConfirmationIfContainsMaster(remoteVariables)
	}

	if remoteVariables.mode == MODE_SQL_FILE {
		return buildLocalFileCommand(remoteVariables.filepath, target)
	}

	if remoteVariables.mode == MODE_SQL_FILE_ON_REMOTE {
		return buildRemoteFileCommand(remoteVariables, target)
	}

	if remoteVariables.mode == MODE_DATABASE {
		return buildDirectDatabaseCommand(target, remoteVariables)
	}

	subCommand := []string{mysqlBinary}
	if !target {
		subCommand[0] = subCommand[0] + "dump"
		subCommand = append(subCommand, "--single-transaction=TRUE")
	}
	subCommand = append(subCommand, buildDatabaseCommandArgumentsWithDatabase(remoteVariables.database)...)
	arguments = append(arguments, strings.Join(subCommand, " "))

	return buildSSHCommand(remoteVariables, arguments)
}

func askForConfirmationIfContainsMaster(remoteVariables remoteVariables) {
	if strings.Contains(strings.ToLower(remoteVariables.database.name), "master") {
		fmt.Println("\033[31m" + "WARNING: Detected \"master\" in targets database name - " + remoteVariables.database.name + "!" + "\033[39m")
		if !askForConfirmation("This could cause data loss!") {
			fmt.Print(" Abort.")
			os.Exit(0)
		}
	}
}

func buildDirectDatabaseCommand(target bool, remoteVariables remoteVariables) *exec.Cmd {
	binary := mysqlDumpBinary
	if target == true {
		binary = mysqlBinary
	}

	return _buildCommand(binary, buildDatabaseCommandArgumentsWithDatabase(remoteVariables.database))
}

func buildSSHCommand(remoteVariables remoteVariables, additionalArguments []string) *exec.Cmd {
	binary := sshBinary
	arguments := getDefaultSSHArguments()
	arguments = append(arguments, remoteVariables.user+"@"+remoteVariables.host)
	arguments = append(arguments, strings.Join(additionalArguments, " "))

	return _buildCommand(binary, arguments)
}

func buildLocalFileCommand(filePath string, target bool) *exec.Cmd {
	if !target {
		return _buildCommand("cat", []string{filePath})
	}

	return exec.Command("false")
}

func buildRemoteFileCommand(remoteVariables remoteVariables, target bool) *exec.Cmd {
	if !target {
		return buildSSHCommand(remoteVariables, []string{"cat", remoteVariables.filepath})
	}

	return exec.Command("false")
}

func _buildCommand(binary string, arguments []string) *exec.Cmd {
	if isVerboseEnabled() {
		fmt.Println("Building Command: " + binary + " " + strings.Join(arguments, " "))
	}

	return exec.Command(binary, arguments...)
}

func getDefaultSSHArguments() []string {
	return []string{
		"-o",
		"StrictHostKeyChecking=no"}
}

func buildDatabaseCommandArguments(database DbConnectionData) []string {
	subCommand := []string{}
	subCommand = append(subCommand, "--default-character-set=utf8")
	subCommand = append(subCommand, "-h"+database.host)
	subCommand = append(subCommand, "-u"+database.user)
	subCommand = append(subCommand, "-p"+database.password)

	return subCommand
}

func buildDatabaseCommandArgumentsWithDatabase(database DbConnectionData) []string {
	subCommand := buildDatabaseCommandArguments(database)
	subCommand = append(subCommand, database.name)

	return subCommand
}
