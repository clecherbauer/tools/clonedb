package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"
)

func executeCommandAndPipeOutputToTempFile(sourceCommand *exec.Cmd) string {
	filename := "/tmp/clone-db." + time.Now().Format("20060102150405")
	outfile, err := os.Create(filename)

	if err != nil {
		panic(err)
	}
	defer outfile.Close()
	stdoutPipe, err := sourceCommand.StdoutPipe()
	writer := bufio.NewWriter(outfile)
	defer writer.Flush()

	err = sourceCommand.Start()
	if err != nil {
		panic(err)
	}
	go io.Copy(writer, stdoutPipe)
	sourceCommand.Wait()

	return filename
}

func pipeFileToRemote(remoteVariables remoteVariables, file string) {
	executePipedCommands(buildLocalFileCommand(file, false), buildCommand(remoteVariables, true))
}

func executePipedCommands(sourceCommand *exec.Cmd, targetCommand *exec.Cmd) {
	reader, writer := io.Pipe()
	sourceCommand.Stdout = writer
	targetCommand.Stdin = reader
	targetCommand.Stderr = os.Stderr
	sourceCommand.Start()
	targetCommand.Start()
	sourceCommand.Wait()
	writer.Close()
	targetCommand.Wait()
	reader.Close()
}

func dropDatabaseIfExists(remoteVariables remoteVariables) {

	dropDbString := "DROP DATABASE IF EXISTS `" + remoteVariables.database.name + "`;"
	executeSQLCommandOnRemote(remoteVariables, dropDbString, false)
}

func createDatabaseIfNotExists(remoteVariables remoteVariables) {

	createDbString := "CREATE DATABASE IF NOT EXISTS `" + remoteVariables.database.name + "`;"
	executeSQLCommandOnRemote(remoteVariables, createDbString, false)
}

func countTablesInDatabase(remoteVariables remoteVariables) string {

	countQuery := "SELECT COUNT(*) FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" + remoteVariables.database.name + "';"
	result := executeSQLCommandOnRemote(remoteVariables, countQuery, false)

	var re = regexp.MustCompile(`(\d+)`)
	for _, match := range re.FindAllString(result, -1) {
		return match
	}

	return "0"
}

func executeSQLCommandOnRemote(remoteVariables remoteVariables, sql string, silent bool) string {
	if remoteVariables.mode == MODE_DATABASE {
		sql = "--execute=" + sql
		arguments := buildDatabaseCommandArguments(remoteVariables.database)
		arguments = append(arguments, sql)
		if silent {
			arguments = append(arguments, "-sN")
		}

		cmd := _buildCommand(mysqlBinary, arguments)
		return executeCommand(cmd, false)
	}

	sql = "--execute=" + "\"" + sql + "\""
	arguments := []string{mysqlBinary}
	arguments = append(arguments, buildDatabaseCommandArguments(remoteVariables.database)...)
	arguments = append(arguments, sql)

	return buildAndExecuteSSHCommand(remoteVariables, arguments)
}

func buildAndExecuteSSHCommand(remoteVariables remoteVariables, arguments []string) string {
	cmd := buildSSHCommand(remoteVariables, arguments)
	return executeCommand(cmd, true)
}

func executeCommand(cmd *exec.Cmd, suppressStdErr bool) string {
	cmdOutput := &bytes.Buffer{}
	if !suppressStdErr {
		cmd.Stderr = os.Stderr
	}
	cmd.Stdout = cmdOutput

	if cmd.Run() != nil {
		fmt.Println("ERROR: could not execute " + cmd.Path + " " + strings.Join(cmd.Args, " "))
		os.Exit(1)
	}
	defer cmd.Wait()

	return cmdOutput.String()
}

func moveFile(sourceFilePath string, targetFilePath string) {
	cmd := _buildCommand("cp", []string{sourceFilePath, targetFilePath})
	executeCommand(cmd, false)
}

func sendFileWithSCP(sourceFilePath string, remoteVariables remoteVariables) {

	targetFilePath := remoteVariables.user + "@" + remoteVariables.host + ":" + remoteVariables.filepath

	scpArguments := getDefaultSSHArguments()
	scpArguments = append(scpArguments, sourceFilePath)
	scpArguments = append(scpArguments, targetFilePath)

	cmd := _buildCommand("scp", scpArguments)
	executeCommand(cmd, false)
	removeFile(sourceFilePath)
}

func removeFile(filePath string) {
	cmd := _buildCommand("rm", []string{filePath})
	executeCommand(cmd, false)
}
