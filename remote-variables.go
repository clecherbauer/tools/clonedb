package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

// DbConnectionData stores mysql connection data
type DbConnectionData struct {
	host     string
	name     string
	user     string
	password string
}

type remoteVariables struct {
	mode     int
	host     string
	user     string
	database DbConnectionData
	dotenv   string
	filepath string
}

const MODE_DATABASE int = 1
const MODE_DATABASE_SSH int = 2
const MODE_DATABASE_SSH_DOTENV int = 3
const MODE_SQL_FILE int = 5
const MODE_SQL_FILE_ON_REMOTE int = 6

func remoteVariableParser(argument string) remoteVariables {
	remoteVariables := remoteVariables{}
	parts := strings.Split(argument, ":")

	if len(parts) == 4 {
		if !strings.Contains(argument, "@") {
			remoteVariables.mode = MODE_DATABASE
			remoteVariables.database.host = parts[0]
		} else {
			remoteVariables.mode = MODE_DATABASE_SSH
			parseAndAppendUserAndHost(parts[0], &remoteVariables)
			remoteVariables.database.host = "localhost"
		}

		remoteVariables.database.user = parts[1]
		remoteVariables.database.password = parts[2]
		remoteVariables.database.name = parts[3]
	}

	if len(parts) == 2 {
		if strings.Contains(argument, ".env") {
			remoteVariables.mode = MODE_DATABASE_SSH_DOTENV
			parseAndAppendUserAndHost(parts[0], &remoteVariables)
			remoteVariables.database.host = "localhost"
			remoteVariables.dotenv = parts[1]
			remoteVariables = parseAndAppendDbConnectionDataFromRemoteEnvFile(remoteVariables)
		} else if strings.Contains(argument, ".sql") {
			remoteVariables.mode = MODE_SQL_FILE_ON_REMOTE
			remoteVariables.filepath = parts[1]
			parseAndAppendUserAndHost(parts[0], &remoteVariables)
		}
	}

	if len(parts) == 1 {
		remoteVariables.mode = MODE_SQL_FILE
		remoteVariables.filepath = parts[0]
	}

	return remoteVariables
}

func parseAndAppendUserAndHost(userHost string, remoteVariables *remoteVariables) {
	parts := strings.Split(userHost, "@")
	remoteVariables.user = parts[0]
	remoteVariables.host = parts[1]
}

func parseAndAppendDbConnectionDataFromRemoteEnvFile(remoteVariables remoteVariables) remoteVariables {

	re := regexp.MustCompile(`^([\w]+)=([\S]+)`)
	lines := strings.Split(buildAndExecuteSSHCommand(remoteVariables, []string{"cat " + remoteVariables.dotenv}), "\n")
	for _, line := range lines {
		matches := re.FindAllStringSubmatch(line, -1)
		for _, submatches := range matches {
			switch submatches[1] {
			case "MYSQL_USER":
				remoteVariables.database.user = submatches[2]
			case "MYSQL_PASSWORD":
				remoteVariables.database.password = submatches[2]
			case "MYSQL_DATABASE":
				remoteVariables.database.name = submatches[2]
			}
		}
	}

	if remoteVariables.database.name == "" {
		fmt.Println("No MYSQL_DATABASE found in " + remoteVariables.dotenv + "!")
		os.Exit(1)
	}

	if remoteVariables.database.password == "" {
		fmt.Println("No MYSQL_PASSWORD found in " + remoteVariables.dotenv + "!")
		os.Exit(1)
	}

	if remoteVariables.database.user == "" {
		fmt.Println("No MYSQL_USER found in " + remoteVariables.dotenv + "!")
		os.Exit(1)
	}

	return remoteVariables
}
