package main

import (
	"fmt"
	"os"
	"strings"
)

// version of this tool
const version = "2.0.1"

var sshBinary = "ssh"
var mysqlBinary = "mysql"
var mysqlDumpBinary = "mysqldump"

func main() {

	if len(os.Args) < 3 {
		printUsage()
		os.Exit(1)
	}
	sourceRemoteVariables := remoteVariableParser(os.Args[1])
	targetRemoteVariables := remoteVariableParser(os.Args[2])

	sourceCommand := buildCommand(sourceRemoteVariables, false)
	targetCommand := buildCommand(targetRemoteVariables, true)

	fmt.Println("Cloning Database ...")

	if targetRemoteVariables.mode == MODE_SQL_FILE_ON_REMOTE {
		tmpPath := executeCommandAndPipeOutputToTempFile(sourceCommand)
		sendFileWithSCP(tmpPath, targetRemoteVariables)
		os.Exit(0)
	}

	if targetRemoteVariables.mode == MODE_SQL_FILE {
		tmpPath := executeCommandAndPipeOutputToTempFile(sourceCommand)
		moveFile(tmpPath, targetRemoteVariables.filepath)
		os.Exit(0)
	}

	dropDatabaseIfExists(targetRemoteVariables)
	createDatabaseIfNotExists(targetRemoteVariables)
	executePipedCommands(sourceCommand, targetCommand)

	if sourceRemoteVariables.mode != MODE_SQL_FILE && sourceRemoteVariables.mode != MODE_SQL_FILE_ON_REMOTE {
		fmt.Println("Result: " + countTablesInDatabase(targetRemoteVariables) + " of " + countTablesInDatabase(sourceRemoteVariables) + " tables cloned")
	}

	if shouldExecutePostSQL() {
		pipeFileToRemote(targetRemoteVariables, getPostSQLFilePath())
	}
}

func shouldExecutePostSQL() bool {
	for _, arg := range os.Args {
		if strings.Contains(arg, "--post-sql") {
			return true
		}
	}

	return false
}

func getPostSQLFilePath() string {
	for _, arg := range os.Args {
		if strings.Contains(arg, "--post-sql=") {
			file := strings.Split(arg, "=")[1]
			return strings.Replace(file, "\"", "", -1)
		}
	}
	return ""
}

func isVerboseEnabled() bool {
	return checkIfArgumentExists("--verbose")
}

func isForceEnabled() bool {
	return checkIfArgumentExists("--force")
}

func checkIfArgumentExists(argumentName string) bool {
	for _, arg := range os.Args {
		if arg == argumentName {
			return true
		}
	}

	return false
}

func readInputFromStdin(fieldlabel string) string {
	var value string
	fmt.Printf("%s: ", fieldlabel)
	fmt.Scanln(&value)
	return value
}

func askForConfirmation(message string) bool {
	userConfirmation := readInputFromStdin(message + " Do you want to continue [Y/n]?")
	return strings.ToUpper(userConfirmation) == "Y"
}

func printUsage() {
	fmt.Println(os.Args[0] + " requires at least 2 arguments")
	fmt.Println("")
	fmt.Println("Usage: " + os.Args[0] + " SOURCE TARGET [options]")
	fmt.Println("")
	fmt.Println("Available Options:")
	fmt.Println(" --verbose					display verbose messages")
	fmt.Println(" --force					dont ask questions")
	fmt.Println(" --post-sql=post.sql				path to sql which should be executed after cloning")
	fmt.Println("")
	fmt.Println("Supported Connection Formats:")
	fmt.Println(" - sql-file						<path-to-sql-file>")
	fmt.Println(" - sql-file on remote				<user>@<host-name>:<path-to-sql-file>")
	fmt.Println(" - direct database access			<host-name>:<db-user>:<db-password>:<database>")
	fmt.Println(" - database at remote via ssh			<user>@<host-name>:<db-user>:<db-password>:<database>")
	fmt.Println(" - database at remote via ssh and dotenv	<user>@<host-name>:<dotenv-path>")
	fmt.Println("")
	fmt.Println("This tool depends on: ssh, scp, mysql, mysqldump, cat, rm, mv")
	fmt.Println("")
	fmt.Println("Version: " + version)
	fmt.Println("")
	fmt.Println("Clone database from Source to Target")
}
