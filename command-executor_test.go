package main

import (
	"testing"
)

func init() {
	sshBinary = "test/mock/ssh"
	mysqlBinary = "test/mock/mysql"
	mysqlDumpBinary = "test/mock/mysqldump"
}

func TestCountTablesInLocalDatabase(t *testing.T) {
	remoteVariables := remoteVariables{}
	remoteVariables.mode = MODE_DATABASE
	remoteVariables.database.host = "dbhost"
	remoteVariables.database.user = "root"
	remoteVariables.database.password = "root"
	remoteVariables.database.name = "database"

	expected := "300"
	actually := countTablesInDatabase(remoteVariables)
	if expected != actually {
		t.Error("TestCountTablesInDatabase was incorrect! " + actually + " does not equal Expected " + expected)
	}
}

func TestCountTablesInRemoteDatabase(t *testing.T) {
	remoteVariables := remoteVariables{}
	remoteVariables.mode = MODE_DATABASE_SSH
	remoteVariables.database.host = "dbhost"
	remoteVariables.database.user = "root"
	remoteVariables.database.password = "root"
	remoteVariables.database.name = "database"

	expected := "200"
	actually := countTablesInDatabase(remoteVariables)
	if expected != actually {
		t.Error("TestCountTablesInDatabase was incorrect! " + actually + " does not equal Expected " + expected)
	}
}
