package main

import (
	"os"
	"testing"
)

func init() {
	sshBinary = "test/mock/ssh"
	mysqlBinary = "test/mock/mysql"
	mysqlDumpBinary = "test/mock/mysqldump"
}

func TestIsVerboseEnabled(t *testing.T) {
	os.Args = []string{"na", "bla", "--verbose"}
	actually := isVerboseEnabled()

	if actually != true {
		t.Error("TestIsVerboseEnabled was incorrect")
	}
}

func TestShouldExecutePostSQL(t *testing.T) {
	os.Args = []string{"na", "bla", "--post-sql=\"test.sql\""}
	actually := shouldExecutePostSQL()

	if actually != true {
		t.Error("TestShouldExecutePostSQL was incorrect")
	}
}

func TestGetPostSQLFilePath(t *testing.T) {
	os.Args = []string{"na", "bla", "--post-sql=\"test.sql\""}
	actually := getPostSQLFilePath()

	if actually != "test.sql" {
		t.Error("TestGetPostSQLFilePath was incorrect")
	}
}

func TestIsForceEnabled(t *testing.T) {
	os.Args = []string{"na", "bla", "--force"}
	actually := isForceEnabled()

	if actually != true {
		t.Error("Force was incorrect")
	}
}
